from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Shoe, BinVO
from common.json import ModelEncoder
import json


class BinVODetailEnccoder(ModelEncoder):
    model = BinVO
    properties = ["closet_name", "bin_number", "import_href", "bin_size", "id"]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ["name","color","manufacturer", "bin","id"]
    encoders = {
        "bin": BinVODetailEnccoder(),
    }


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "name",
        "color",
        "picture_url",
        "bin",
    ]
    encoders = {
        "bin": BinVODetailEnccoder(),
    }


@require_http_methods(["GET", "POST", "DELETE"])
def api_list_shoes(request, shoes_vo_id=None):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            shoes_href = content['bin']
            bin = BinVO.objects.get(import_href=shoes_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        content = json.loads(request.body)
        try:
            id = content['id']
            shoe = Shoe.objects.get(id=id)
        except Shoe.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
        shoe.delete()
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
