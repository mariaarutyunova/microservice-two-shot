from django.db import models
from django.urls import reverse


# Create your models here.
class Shoe(models.Model):
    """
    The Location model describes the place at which an
    Event takes place, like a hotel or conference center.
    """
    name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.URLField(null=True)
    manufacturer = models.CharField(max_length=200)
    bin = models.ForeignKey(
        "BinVO",
        related_name="bin",
        on_delete=models.PROTECT,
    )

    def get_api_url(self):
        return reverse("api_show_shoe", kwargs={"pk": self.pk})

    def __str__(self):
        return self.name


class BinVO(models.Model):
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=200, unique=True)
