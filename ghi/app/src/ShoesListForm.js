import React, { useEffect, useState } from 'react';
import { NavLink } from "react-router-dom";

function ShoesListForm () {

    const [shoes, setShoe] = useState('');

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/shoes';
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setShoe(data.shoes)
        }
      }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>manufacturer</th>
              <th>color</th>
              <th>closet_name</th>
              <th>bin_number</th>
              <th>bin_size</th>
              <th> <NavLink className="nav-link" to="/shoes/new">Add</NavLink> or Delete</th>
            </tr>
          </thead>
          <tbody>
          {Array.from(shoes).map((shoe) => {
            return (
                <tr key={shoe.href}>
                  <td>{shoe.name}</td>
                  <td>{shoe.manufacturer}</td>
                  <td>{shoe.color}</td>
                  <td>{shoe.bin.closet_name}</td>
                  <td>{shoe.bin.bin_number}</td>
                  <td>{shoe.bin.bin_size}</td>
                  <td>
                    <NavLink className="nav-link" to="/shoes/delete" state={{shoe}}>Delete</NavLink>
                  </td>
                </tr>
                );
            })}
          </tbody>
        </table>
      );
    }
export default ShoesListForm;
