import React from 'react';
import { useLocation } from 'react-router-dom';


function DeleteShoesForm () {
    let location = useLocation();
    const del_id = location.state.shoe.id

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.id = del_id
        const locationUrl = 'http://localhost:8080/api/shoes/';

        const fetchConfig = {
          method: "delete",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
          alert("Success delete!")
        }
    }

    return(
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new shoe</h1>
            <form onSubmit={handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                <p>Are you sure to delete?</p>
              </div>
              <button className="btn btn-primary">Delete</button>
            </form>
          </div>
        </div>
      </div>
    );


}
export default DeleteShoesForm;
