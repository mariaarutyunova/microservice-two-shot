import React, { useEffect, useState } from 'react';


function NewShoesForm () {

    const [bins, setBins] = useState('');
    const [name, setName] = useState('');
    const [color, setColor] = useState('');
    const [manufacturer, setManufacturer] = useState('');
    const [picture_url, setPicture_url] = useState('');
    const [binsVO, setBinsVO] = useState('');



    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setBins(data.bins);
        }
      }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.manufacturer = manufacturer;
        data.name = name;
        data.color = color;
        data.picture_url = picture_url;
        data.bin = binsVO
        const locationUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
          const newShoe = await response.json();
          setName('');
          setColor('');
          setManufacturer('');
          setPicture_url('');
          setBinsVO('');
        }
    }

    useEffect(() => {
        fetchData();
      }, []);

    const handleBinChange = (event) => {
        const value = event.target.value;
        setBinsVO(value);
      }

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
      }

      const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
      }

      const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
      }

      const handlePicture_urlChange = (event) => {
        const value = event.target.value;
        setPicture_url(value);
      }

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new shoe</h1>
            <form onSubmit={handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="name" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleManufacturerChange}  value={manufacturer} placeholder="manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
                <label htmlFor="manufacturer">Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleColorChange} value={color}   placeholder="color" required type="text" name="color" id="color" className="form-control"/>
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePicture_urlChange} value={picture_url}  placeholder="picture_url" required type="text" name="picture_url" id="picture_url" className="form-control"/>
                <label htmlFor="picture_url">Picture_url</label>
              </div>
              <div className="mb-3">
                <select required onChange={handleBinChange} value={binsVO}  name="binsVO" id="binsVO" className="form-select">
                    <option value="">Choose a bin</option>
                        {Array.from(bins).map((bin:any, index: number) => {
                      return (
                        <option key={index} value={bin.href}>{bin.closet_name}</option>
                      )
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default NewShoesForm;
