import React, { useState, useEffect } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import NewShoesForm from './NewShoesForm';
import ShoesListForm from './ShoesListForm';
import DeleteShoesForm from './DeleteShoesForm';
import HatList from './HatList';
import NewHatForm from './NewHatForm';


function App(props) {
  const [hats, setHats] = useState([]);
  const [locations, setLocations] = useState([]);

  async function loadHats() {
    const response = await fetch('http://localhost:8090/api/hats/');
    if (response.ok) {
      const data = await response.json();
      setHats(data.hats);
    } else {
      console.error(response);
    }
  }

  async function loadLocations() {
    const response = await fetch('http://localhost:8090/api/locations/');
    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    } else {
      console.error(response);
    }
  }

  useEffect(() => {
    loadHats();
    loadLocations();
    console.log('Use effect fired!');
  }, []);

  const addHat = async (newHat) => {
    try {
      const response = await fetch('http://localhost:8090/api/hats/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(newHat),
      });

      if (response.ok) {
        const data = await response.json();
        setHats([...hats, data.hat]);
      } else {
        console.error(response);
      }
    } catch (error) {
      console.error(error);
    }
  };

  // if (hats === undefined || locations === undefined) {
  //   return null;
  // }

  console.log('React rendered');

  return (
    <BrowserRouter>
      <Nav />
      <Routes>
        <Route path="/" element={<MainPage />} />
        <Route path="hats" element={<HatList hats={hats} />} />
        <Route path="new-hat" element={<NewHatForm addHat={addHat} locations={locations}/>}/>
      <Route path="shoes">
              <Route path='list' element={<ShoesListForm />}></Route>
              <Route path='new' element={<NewShoesForm />}></Route>
              <Route path='delete' element={<DeleteShoesForm />}></Route>
      </Route>
      </Routes>
    </BrowserRouter>
  );

}

export default App;
