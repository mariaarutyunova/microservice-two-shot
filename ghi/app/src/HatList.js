import { Link } from "react-router-dom";

// if we say key we need key in the URL. Otherwise we can keep ID
function HatList(props) {
    const handleDelete = (id)=>{
    fetch (`http://localhost:8090/api/hats/${id}/`,{
        method:'DELETE'
    }).then((response)=>response.json()).then((data)=>{console.log(data)})
    }


  return (
    <>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Style Name</th>
            <th>Fabric</th>
            <th>Color</th>
            <th>Location</th>
          </tr>
        </thead>
        <tbody>
          {props.hats.map((hat,key) => {
            return(
            <tr key={key}>
              <td>{hat.style_name}</td>
              <td>{hat.fabric}</td>
              <td>{hat.color}</td>
              <td>{hat.location.closet_name}</td>
              <td className="btn btn-danger text-black" onClick={()=>{handleDelete(hat.id)}}>Delete </td>
            </tr>
            );
        })}
        </tbody>
      </table>
      <div className="text-center">
      <Link to ="/new-hat" className="btn btn-primary">Create a New Hat!</Link>
      </div>
      </>

  );
}

export default HatList;
