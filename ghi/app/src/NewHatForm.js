import React, { useState } from 'react';

function NewHatForm(props) {
  const [styleName, setStyleName] = useState('');
  const [fabric, setFabric] = useState('');
  const [color, setColor] = useState('');
  const [location, setLocation] = useState('');
  const [pictureUrl, setPictureUrl] = useState('');

  const handleSubmit = (event) => {
    event.preventDefault();
    console.log("location", location)
    const newHat = {
      style_name: styleName,
      fabric: fabric,
      color: color,
      picture_url: pictureUrl,
      location: location,
    };

    props.addHat(newHat);

    setStyleName('');
    setFabric('');
    setColor('');
    setLocation('');
    setPictureUrl('');
  };

  return (
    <div className="container">
      <div className="row justify-content-center">
        <div className="col-md-6">
          <form onSubmit={handleSubmit}>
            <h3>Add a New Hat</h3>
            <div className="form-group">
              <label>Style Name:</label>
              <input
                type="text"
                className="form-control"
                value={styleName}
                onChange={(event) => setStyleName(event.target.value)}
              />
            </div>
            <div className="form-group">
              <label>Fabric:</label>
              <input
                type="text"
                className="form-control"
                value={fabric}
                onChange={(event) => setFabric(event.target.value)}
              />
            </div>
            <div className="form-group">
              <label>Color:</label>
              <input
                type="text"
                className="form-control"
                value={color}
                onChange={(event) => setColor(event.target.value)}
              />
            </div>
            <div className="form-group">
              <label>Picture URL:</label>
              <input
                type="text"
                className="form-control"
                value={pictureUrl}
                onChange={(event) => setPictureUrl(event.target.value)}
              />
            </div>
            <div className="form-group">
              <label>Location:</label>
              <select
                className="form-select"
                onChange={(event) => setLocation(event.target.value)}
              >
                <option value="">Select a location</option>
                {props.locations.map((location) => (
                  <option key={location.id} value={location.import_href}>
                    {location.closet_name}
                  </option>
                ))}
              </select>
            </div>
            <button type="submit" className="btn btn-primary">
              Add Hat
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default NewHatForm;
