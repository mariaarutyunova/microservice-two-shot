# Wardrobify

Team:
Maria Arutyunova Hat microservice
Liangjian Chen Shoe microservice

* Person 1 - Which microservice?
* Person 2 - Which microservice?

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.
Create a view funtion for showing the shoe list, create shoe and delete shoe. Getting data for bin by using poller. Useing React to create a front-end SPAs. Using router to direct each Urls.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

Create hat URLs, views, and models.
hats/poll is a polling application that uses the Django resources in the RESTful API project. It contains a script file, hats/poll/poller.py, that you must implement to pull Location data from the Wardrobe API.
